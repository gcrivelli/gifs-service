# GIFS Service

## Como utilizarlo
- Clone the repository with __git clone__
- Copy __docker-compose.yml.example__ file to __docker-compose.yml__
- Run __docker-compose up -d__
- Run __docker exec -it gifs-service bash__
- Copy __.env.example__ file to __.env__
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__
- Run __php artisan passport:client --password__
- Set __GIPHY_APP_KEY environment__
- Set __PASSPORT_CLIENT_ID environment__
- Set __PASSPORT_CLIENT_SECRET environment__
- Run __chown -R www-data:www-data storage/__
- Run __exit__ (salimos del contenedor)

## Documentación
- [Diagrama de Casos de Uso](docs/Diagramas%20Casos%20de%20Uso.png)
- [Diagrama de Secuencia](docs/Diagrama de Secuencia.md)
- [Diagrama de Datos](docs/Diagrama%20de%20Datos.png)
- [Collección POSTMAN](docs/GIFS%20Service.postman_collection.json)

## Persistencia de Peticiones
Para persistir las peticiones, utilice Telescope y se puede acceder [aquí](http://localhost/telescope/)

## Ejecutar Test
- php artisan test

## En caso de modificar las variables de ambiente
- php artisan config:cache && php artisan config:clear

## Debug
- Se encuentra instalado el xdebug el cual necesita que el idea a utilizar escuche en el puerto 9003 y configurar la conexión remota 