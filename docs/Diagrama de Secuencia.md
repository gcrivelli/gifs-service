```mermaid
%% Sequence Diagram for `login` Endpoint
sequenceDiagram
    participant User
    participant LoginController
    
    User ->> LoginController: Solicitar Token
    activate LoginController

    LoginController ->> User: Devolver access_token
    deactivate LoginController
```
```mermaid
%% Sequence Diagram for `search gifs` Endpoint
sequenceDiagram
    participant User
    participant GifsApiController
    participant GifService
    participant GIPHYService
    participant APIGiphy
    
    User ->> GifsApiController: Buscar GIFS
    activate GifsApiController
    
    GifsApiController ->> GifService: Buscar
    activate GifService

    GifService ->> GIPHYService: Buscar
    activate GIPHYService

    GIPHYService ->> APIGiphy: Buscar GIFs
    activate APIGiphy

    APIGiphy ->> GIPHYService: Devolver lista de GIFs
    deactivate APIGiphy

    GIPHYService ->> GifService: Devolver lista de GIFs
    deactivate GIPHYService

    GifService ->> GifsApiController: Devolver lista de GIFs
    deactivate GifService

    GifsApiController ->> User: Devolver lista de GIFs
    deactivate GifsApiController
```
```mermaid
%% Sequence Diagram for `search gifs by id` Endpoint
sequenceDiagram
    participant User
    participant GifsApiController
    participant GifService
    participant GIPHYService
    participant APIGiphy
    
    User ->> GifsApiController: Buscar GIF por ID
    activate GifsApiController
    
    GifsApiController ->> GifService: Buscar por ID
    activate GifService

    GifService ->> GIPHYService: Buscar por ID
    activate GIPHYService

    GIPHYService ->> APIGiphy: Buscar GIF por ID
    activate APIGiphy

    APIGiphy ->> GIPHYService: Devolver GIF
    deactivate APIGiphy

    GIPHYService ->> GifService: Devolver GIF
    deactivate GIPHYService

    GifService ->> GifsApiController: Devolver GIF
    deactivate GifService

    GifsApiController ->> User: Devolver GIF
    deactivate GifsApiController
```
```mermaid
%% Sequence Diagram for `store favorite gif` Endpoint
sequenceDiagram
    participant User
    participant GifsApiController
    participant GifService
    participant GIPHYService
    participant APIGiphy
    
    User ->> GifsApiController: Guardar GIF favorito
    activate GifsApiController
    
    GifsApiController ->> GifService: Guardar GIF favorito
    activate GifService

    GifService ->> GIPHYService: Buscar por ID
    activate GIPHYService

    GIPHYService ->> APIGiphy: Buscar GIF por ID
    activate APIGiphy

    APIGiphy ->> GIPHYService: Devolver GIF
    deactivate APIGiphy

    GIPHYService ->> GifService: Devolver GIF
    deactivate GIPHYService

    GifService ->> GifService: Guardar GIF favorito

    GifService ->> GifsApiController: Devolver Favorito
    deactivate GifService

    GifsApiController ->> User: Devolver Favorito
    deactivate GifsApiController
```