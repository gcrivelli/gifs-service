<?php

namespace App\Services;

use App\Models\User;
use App\Services\Providers\GIPHYService;
use Exception;
use Illuminate\Database\Eloquent\Model;

class GifService
{
    protected GIPHYService $giphyService;
    public function __construct(GIPHYService $giphyService)
    {
        $this->giphyService = $giphyService;
    }

    /**
     * @throws Exception
     */
    public function search(string $query, int $limit = 20, int $offset = 5)
    {
        return $this->giphyService->search($query, $limit, $offset);
    }

    /**
     * @throws Exception
     */
    public function searchById(string $gif_id)
    {
        return $this->giphyService->searchById($gif_id);
    }

    /**
     * @throws Exception
     */
    public function storeFav(string $gif_id, string $alias, User $user): Model
    {
        return $user->favs()->create([
            'alias' => $alias,
            'gif_id' => $this->giphyService->searchById($gif_id)['data']['id']
        ]);
    }
}
