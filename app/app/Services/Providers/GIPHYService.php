<?php

namespace App\Services\Providers;

use Exception;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\HttpException;


class GIPHYService
{
    protected string $base_url;

    public function __construct()
    {
        $this->base_url = 'https://api.giphy.com/v1/';
    }

    /**
     * @throws Exception
     */
    public function responseCodes($response)
    {
        $meta = $response['meta'];

        //TODO: por seguridad y privacidad de los datos, si la respuesta no es 200, es necesario logear el error para analizarlo posteriormente y al usuario final responder con un error interno

        if($meta['status'] != 200) throw new HttpException($meta['status'], $meta['msg']);

        unset($response['meta']);

        return $response;
    }

    /**
     * @throws Exception
     */
    public function getData(string $url, array $params = [])
    {
        $params['api_key'] = env('GIPHY_APP_KEY');

        return $this->responseCodes(Http::asForm()->get($this->base_url . $url, $params)->json());
    }

    /**
     * @throws Exception
     */
    public function search(string $query, int $limit = 20, int $offset = 5)
    {
        return $this->getData('gifs/search', [
            'q' => $query,
            'limit' => $limit,
            'offset' => $offset,
        ]);
    }

    /**
     * @throws Exception
     */
    public function searchById(string $gif_id)
    {
        return $this->getData('gifs/' . $gif_id);
    }
}
