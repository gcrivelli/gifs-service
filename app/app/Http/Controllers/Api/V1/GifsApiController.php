<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\IndexGifRequest;
use App\Http\Requests\StoreFavGifRequest;
use App\Http\Resources\GifResource;
use App\Services\GifService;
use Exception;

class GifsApiController extends Controller
{

    protected GifService $gifService;

    /**
     * Create a new controller instance.
     */
    public function __construct(GifService $gifService)
    {
        $this->gifService = $gifService;
    }

    /**
     * @throws Exception
     */
    public function index(IndexGifRequest $request): GifResource
    {
        return new GifResource($this->gifService->search($request->get('query'), $request->get('limit', 20), $request->get('offset', 0)));
    }

    /**
     * @throws Exception
     */
    public function show(string $gif_id): GifResource
    {
        return new GifResource($this->gifService->searchById($gif_id));
    }

    /**
     * @throws Exception
     */
    public function fav(StoreFavGifRequest $request): GifResource
    {
        return new GifResource($this->gifService->storeFav($request->get('gif_id'), $request->get('alias'), $request->get('user')));
    }

}
