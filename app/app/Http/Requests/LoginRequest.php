<?php

namespace App\Http\Requests;

use App\Models\User;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use League\OAuth2\Server\Exception\OAuthServerException;

class LoginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => [
                'string',
                'required',
            ],
            'device_name' => [
                'string',
                'nullable',
            ],
        ];
    }

    /**
     * @throws Exception
     */
    public function passedValidation(): void
    {
        //if(!Auth::attempt($this->only('email', 'password'))) throw OAuthServerException::invalidCredentials();
    }

}
