<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreFavGifRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'gif_id' => [
                'string',
                'required',
            ],
            'alias' => [
                'string',
                'required',
            ],
            'user_id' => [
                'integer',
                'exists:users,id',
                'required',
            ],
        ];
    }

    public function passedValidation(): void
    {
        $this->query->add([
            'user' => User::where('id', $this->get('user_id'))->first(),
        ]);
    }
}
