<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexGifRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'query' => [
                'string',
                'required',
            ],
            'limit' => [
                'integer',
                'nullable',
                'min:1',
                'max:50'
            ],
            'offset' => [
                'integer',
                'nullable',
                'min:0',
                'max:4999'
            ],
        ];
    }

}
