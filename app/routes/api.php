<?php

use App\Http\Controllers\Api\V1\GifsApiController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth', 'as' => 'auth.', 'namespace' => 'Auth'], function () {
    Route::post('login', [LoginController::class, 'login'])->name('login');
});

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1', 'middleware' => ['auth:api']], function () {

    Route::group(['prefix' => 'gifs', 'as' => 'gifs.'], function () {
        Route::get('', [GifsApiController::class, 'index'])->name('index');
        Route::get('{gif_id}', [GifsApiController::class, 'show'])->name('show');
        Route::post('fav', [GifsApiController::class, 'fav'])->name('fav.store');
    });

});
