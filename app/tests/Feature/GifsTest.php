<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class GifsTest extends TestCase
{

    public function test_search_gifs()
    {
        Passport::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get(route('api.gifs.index', [
            'limit' => rand(1, 50),
            'offset' => 0,
            'query' => 'soccer'
        ]));

        $response->assertStatus(200)->assertJsonStructure(['data', 'pagination']);
    }

    public function test_search_gifs_with_bad_parameters()
    {
        Passport::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get(route('api.gifs.index', [
            'limit' => 646678,
            'offset' => -4,
            'query' => 'sfdggjytrewadszxcghfytrdesdzfxfhf'
        ]));

        $response->assertStatus(422)->assertJsonStructure(['message', 'errors']);
    }


    public function test_search_gif_by_id()
    {
        Passport::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get(route('api.gifs.show', [
            'gif_id' => 'xL8cQyKonCqjDHYYPS'
        ]));

        $response->assertStatus(200)->assertJsonStructure(['data']);
    }

    public function test_save_my_favorite_soccer_gif()
    {
        $user = Passport::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get(route('api.gifs.index', [
            'limit' => 2,
            'offset' => 0,
            'query' => 'soccer'
        ]));

        $gifs_id = $response->collect(['data'])->first()['id'];

        $response = $this->post(route('api.gifs.fav.store'), [
            'gif_id' => $gifs_id,
            'alias' => 'Mi Gif Favorito',
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('favs', [
            'id' => $response->collect(['data'])['id'],
        ]);

    }
}
